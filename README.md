## TasksAppFrontend

This is a private repo containing the frontend of BeeOrder's Recruitment Test.

## Usage

- Make sure to have the backend and database set up and running.

- Run the following command: `ng serve`
