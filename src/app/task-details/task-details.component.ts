import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subtask } from '../shared/models/subtasks.model';
import { Task } from '../shared/models/task.model';
import { TaskService } from '../shared/services/task.service';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';

@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.css'],
})
export class TaskDetailsComponent implements OnInit {
  task: Task;

  creationDate: string;

  @ViewChild('mySwal')
  public readonly mySwal!: SwalComponent;

  constructor(
    private route: ActivatedRoute,
    private taskService: TaskService,
    private datePipe: DatePipe,
    private router: Router
  ) {}

  ngOnInit(): void {
    const taskId = +this.route.snapshot.paramMap.get('id')!;

    this.taskService.getTaskById(taskId).subscribe((task) => {
      this.task = task;
      const dateObject = new Date(this.task.createdAt! as unknown as string);
      this.creationDate = this.datePipe.transform(
        dateObject,
        'YYYY-MM-ddThh:mm'
      )!;
    });
  }

  get subtasks() {
    return this.task.subtasks as Subtask[];
  }

  isTaskDefined(): boolean {
    return !!this.task;
  }

  handleDelete() {
    console.log('here');
    this.taskService.deleteTask(this.task.id).subscribe((_) => {
      this.router.navigate(['/tasks']);
    });
  }
}
