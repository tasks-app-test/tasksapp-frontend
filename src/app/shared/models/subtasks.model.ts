export interface Subtask {
  id: number;
  description: string;
  taskId: number;
}
