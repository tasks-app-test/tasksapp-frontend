import { Category } from './category.model';
import { Subtask } from './subtasks.model';

export interface Task {
  id: number;
  description: string;
  deadline: Date;
  subtasks: string[] | Subtask[];
  categories: Category[];
  isEnded: boolean;
  createdAt?: Date;
}
