import { Component } from '@angular/core';
import { Task } from '../shared/models/task.model';
import { TaskService } from '../shared/services/task.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css'],
})
export class TasksComponent {
  tasks: Task[] = [];

  searchText: string = '';

  constructor(private taskService: TaskService) {}

  ngOnInit() {
    this.taskService.getTasks().subscribe((data) => {
      this.tasks = data;
    });
  }

  filterTasks(completed: boolean): Task[] {
    return this.tasks.filter(
      (task) =>
        task.isEnded == completed &&
        task.description.toLowerCase().includes(this.searchText.toLowerCase())
    );
  }
}
