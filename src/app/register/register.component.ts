import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: [
    './register.component.css',
    '../../assets/styles/auth-styles.css',
  ],
})
export class RegisterComponent {
  constructor(private router: Router, private authService: AuthService) {}

  registerForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.min(8)]),
  });

  isLoading = false;
  error = '';

  onSubmit() {
    console.log(this.registerForm.value);
    this.error = '';
    this.isLoading = true;
    this.authService
      .register(
        this.registerForm.controls.name.value!,
        this.registerForm.controls.email.value!,
        this.registerForm.controls.password.value!
      )
      .subscribe(
        (response) => {
          console.log('Register successful', response);
          localStorage.setItem('authToken', response.token);
          this.authService.notifyStateChange(true);
          this.router.navigate(['/tasks']);
        },
        (error) => {
          console.error('Register error', error);
          this.error =
            'Please make sure to fill all fields correctly and try again.';
          this.isLoading = false;
        }
      );
  }
}
