import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../shared/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css', '../../assets/styles/auth-styles.css'],
})
export class LoginComponent {
  constructor(private router: Router, private authService: AuthService) {}

  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
  });

  isLoading = false;

  error = '';

  onSubmit() {
    console.log(this.loginForm.value);
    this.isLoading = true;
    this.error = '';
    this.authService
      .login(
        this.loginForm.controls.email.value!,
        this.loginForm.controls.password.value!
      )
      .subscribe(
        (response) => {
          console.log('Login successful', response);
          localStorage.setItem('authToken', response.token);
          this.authService.notifyStateChange(true);
          this.router.navigate(['/tasks']);
        },
        (error) => {
          console.error('Login error', error);
          this.error =
            "Can't login. Please check your credentials and try again.";
          this.isLoading = false;
          // Swal.fire({
          //   text: "Can't login. Please check your credentials and try again.",
          // });
        }
      );
  }
}
