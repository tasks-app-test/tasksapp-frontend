import { Component, Input } from '@angular/core';
import { Task } from '../shared/models/task.model';
import { TaskService } from '../shared/services/task.service';

@Component({
  selector: 'app-task-list-item',
  templateUrl: './task-list-item.component.html',
  styleUrls: ['./task-list-item.component.css'],
})
export class TaskListItemComponent {
  @Input() task: Task;

  constructor(private taskService: TaskService) {}

  formatDate(inputDateStr: string) {
    // Parse the input date string into a Date object
    const inputDate = new Date(inputDateStr);

    // Extract day, month, and year components
    const day = inputDate.getDate().toString().padStart(2, '0');
    const month = (inputDate.getMonth() + 1).toString().padStart(2, '0');
    const year = inputDate.getFullYear().toString().substr(-2);

    // Format the components into "dd-MM-yy" format
    const formattedDate = `${day}-${month}-${year}`;

    return formattedDate;
  }

  handleToggle() {
    this.taskService.toggleIsEnded(this.task.id).subscribe((response) => {
      console.log(response);
    });
    this.task.isEnded = !this.task.isEnded;
  }
}
