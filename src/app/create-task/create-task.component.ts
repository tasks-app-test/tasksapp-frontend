import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms'; // Import form-related modules
import { Task } from '../shared/models/task.model';
import { Category } from '../shared/models/category.model';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { CategoryService } from '../shared/services/category.service';
import { Observable } from 'rxjs';
import { TaskService } from '../shared/services/task.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.css'],
})
export class CreateTaskComponent {
  faTrash = faTrash;

  taskForm: FormGroup; // Create a FormGroup for the entire form
  newTask: Task; // Define a newTask object

  selectedCategories = [];
  categoriesList$: Observable<Category[]>;

  todayString = new Date().toISOString().substr(0, 10);

  isLoading = false;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private categoryService: CategoryService,
    private taskService: TaskService
  ) {
    // Initialize the form structure
    this.taskForm = this.formBuilder.group({
      description: ['', Validators.required],
      deadline: [this.todayString, Validators.required],
      subtasks: this.formBuilder.array([]),
      isEnded: [false, Validators.required],
    });

    this.newTask = {
      id: 0,
      description: '',
      deadline: new Date(),
      subtasks: [],
      categories: [],
      isEnded: false,
    };
  }

  ngOnInit() {
    this.categoriesList$ = this.categoryService.getCategories();
  }

  get subtasks(): FormArray {
    return this.taskForm.get('subtasks') as FormArray;
  }

  addSubtask() {
    this.subtasks.push(this.formBuilder.control(''));
  }

  removeSubtask(index: number) {
    this.subtasks.removeAt(index);
  }

  onSubmit() {
    if (this.taskForm.valid && this.selectedCategories.length > 0) {
      this.newTask.description = this.taskForm.get('description')!.value;
      this.newTask.deadline = this.taskForm.get('deadline')!.value;
      this.newTask.subtasks = this.taskForm.get('subtasks')!.value;
      this.newTask.categories = this.selectedCategories;
      this.newTask.isEnded = this.taskForm.get('isEnded')!.value;

      this.isLoading = true;
      this.taskService.createTask(this.newTask).subscribe(
        (response) => {
          console.log('Added successful', response);
          this.router.navigate(['/tasks']);
        },
        (error) => {
          console.error('Creation error', error);
          this.isLoading = false;
        }
      );
    } else {
      // Form is invalid; display validation errors if needed
    }
  }
}
