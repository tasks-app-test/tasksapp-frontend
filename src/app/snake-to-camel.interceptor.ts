import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpResponse,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class SnakeToCamelInterceptor implements HttpInterceptor {
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      map((event) => {
        if (event instanceof HttpResponse) {
          return this.convertResponseDataToCamelCase(event);
        }
        return event;
      })
    );
  }

  private convertResponseDataToCamelCase(
    response: HttpResponse<any>
  ): HttpResponse<any> {
    // Check if the response contains data
    if (response.body) {
      const camelCaseData = this.convertKeysToCamelCase(response.body);
      return response.clone({ body: camelCaseData });
    }
    return response;
  }

  private convertKeysToCamelCase(data: any): any {
    if (Array.isArray(data)) {
      return data.map((item) => this.convertKeysToCamelCase(item));
    } else if (typeof data === 'object' && data !== null) {
      const camelCaseData: { [key: string]: any } = {};
      for (const key in data) {
        if (data.hasOwnProperty(key)) {
          const camelCaseKey = this.snakeToCamel(key);
          camelCaseData[camelCaseKey] = this.convertKeysToCamelCase(data[key]);
        }
      }
      return camelCaseData;
    }
    return data;
  }

  private snakeToCamel(key: string): string {
    return key.replace(/_([a-z])/g, (match) => match[1].toUpperCase());
  }
}
